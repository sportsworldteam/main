package com.semicolon.sportsworld.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.semicolon.sportsworld.R;
import com.semicolon.sportsworld.classes.AngleFont;
import com.semicolon.sportsworld.classes.Kenyan_coffee;
import com.semicolon.sportsworld.database.SportsWorldDbSchema;
import com.semicolon.sportsworld.fragments.MapFragment;
import com.semicolon.sportsworld.fragments.TeamsAvailable;

public class MapsActivity extends AppCompatActivity{

    LinearLayout currently_clicked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        Typeface typeface = Kenyan_coffee.getInstance(getApplicationContext()).kenyan_coffee_bold;
        Typeface kenyanCoffeeRegularIt = Kenyan_coffee.getInstance(getApplicationContext()).kenyan_coffee_regular;
        Typeface profile_subtitle = AngleFont.getInstance(getApplicationContext()).wideFont;
        TextView userProfileName = findViewById(R.id.user_profile_name);
        TextView textView = findViewById(R.id.user_subtitle);
        userProfileName.setTypeface(typeface);
        userProfileName.setText(R.string.dumy_profile_name);

        textView.setTypeface(profile_subtitle);

//        ImageView profileImage = findViewById(R.id.profile_pic);
        Bitmap bitmap = getCroppedBitmap(
                BitmapFactory.decodeResource(getResources(),R.drawable.dummy_profile_pic)
        );

        Drawable drawable = new BitmapDrawable(getResources(),bitmap);
//        profileImage.setImageDrawable(drawable);

        View view = findViewById(R.id.btn_1);
        TextView textView1 = view.findViewWithTag(getResources().getString(R.string.text_drawer_list));
        textView1.setTypeface(kenyanCoffeeRegularIt);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickDrawerBtn(v);
            }
        });

        View view2 = findViewById(R.id.btn_2);
        view2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickDrawerBtn(v);
            }
        });
        TextView textView2 = view2.findViewWithTag(getResources().getString(R.string.text_drawer_list));
        textView2.setTypeface(kenyanCoffeeRegularIt);

        View view4 = findViewById(R.id.btn_4);
        view4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickDrawerBtn(v);
            }
        });
        TextView textView4 = view4.findViewWithTag(getResources().getString(R.string.text_drawer_list));
        textView4.setTypeface(kenyanCoffeeRegularIt);

        View view3 = findViewById(R.id.btn_3);
        view3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickDrawerBtn(v);
            }
        });
        TextView textView3 = view3.findViewWithTag(getResources().getString(R.string.text_drawer_list));
        textView3.setTypeface(kenyanCoffeeRegularIt);

        currently_clicked = null;

        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, new MapFragment(), MapFragment.TAG)
                .commit();


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);


    }

    public void onClickDrawerBtn(View view)
    {

        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        drawerLayout.closeDrawer(Gravity.START);

        if(currently_clicked != view){
            view.setBackground(getResources().getDrawable(R.drawable.drawer_list_area_gradient));
            TextView textView = view.findViewWithTag(getString(R.string.text_drawer_list));
            textView.setTextColor(getResources().getColor(R.color.colorAccentLight));
            ImageView imageView = view.findViewWithTag(getString(R.string.img_drawer_list));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                view.setElevation(view.getElevation() - 8);
            }

            if(currently_clicked != null)
            {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    currently_clicked.setElevation(currently_clicked.getElevation() + 8);
                }

                currently_clicked.setBackground(getResources().getDrawable(R.drawable.border));
//                currently_clicked.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                TextView textView1 = currently_clicked.findViewWithTag(getString(R.string.text_drawer_list));
                textView1.setTextColor(Color.WHITE);
                ImageView imageView1 = currently_clicked.findViewWithTag(getString(R.string.img_drawer_list));
            }

            currently_clicked = (LinearLayout) view;

            String selected = ((TextView)view.findViewWithTag(getResources().getString(R.string.text_drawer_list))).getText().toString();
            if(selected.equals("Teams")) {
                getSupportFragmentManager().beginTransaction()
                        .remove(getSupportFragmentManager().findFragmentByTag(MapFragment.TAG))
                        .add(R.id.fragment_container, TeamsAvailable.newInstance(), TeamsAvailable.TAG)
                        .commit();
            }
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

    public Bitmap getCroppedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xFF545454;
        final Paint paint = new Paint();
        paint.setStrokeWidth(6);
        final Rect rect = new Rect(0, 0,
                bitmap.getWidth() < bitmap.getHeight()
                        ? bitmap.getWidth()
                        : bitmap.getHeight(),
                bitmap.getWidth() < bitmap.getHeight()
                        ? bitmap.getWidth()
                        : bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() < bitmap.getHeight() ?
                        bitmap.getWidth()/ 2 :
                        bitmap.getHeight()/2, paint);
//        Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
//        return _bmp;
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

}
