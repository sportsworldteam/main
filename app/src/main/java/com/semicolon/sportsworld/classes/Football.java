package com.semicolon.sportsworld.classes;

/**
 * Created by Taha Malik on 08/07/2018.
 **/
public class Football extends Sport {

    public final static String ATTACKER = "Attacker";
    public final static String MIDFIELDER = "MidFielder";
    public final static String DEFENDER = "Defender";
    public final static String GOALKEEPER = "GoalKeeper";


    public String position;

    protected Football(PreferredArm preferredArm, String position) {
        super("Football", preferredArm);
        this.position = position;

    }
}
