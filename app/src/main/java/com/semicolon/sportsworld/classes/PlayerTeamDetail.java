package com.semicolon.sportsworld.classes;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Taha Malik on 08/04/2018.
 **/
public class PlayerTeamDetail {

    public String team_name;
    public String position;

    public PlayerTeamDetail(String team_name, String position)
    {
        this.team_name = team_name;
        this.position = position;
    }



}
