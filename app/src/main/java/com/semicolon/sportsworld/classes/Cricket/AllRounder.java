package com.semicolon.sportsworld.classes.Cricket;

import com.semicolon.sportsworld.classes.PreferredArm;

/**
 * Created by Taha Malik on 08/07/2018.
 **/
public class AllRounder extends Cricket{


    public String battingOrder;
    public String bowlingType;
    public String battingType;

    protected AllRounder(PreferredArm preferredArm, String order, String battingType, String bowlingType) {
        super("Cricket", preferredArm);
        this.battingOrder = order;
        this.battingType = battingType;
        this.bowlingType = bowlingType;
    }
}
