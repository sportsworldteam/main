package com.semicolon.sportsworld.classes;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by Taha Malik on 08/03/2018.
 **/
public class Kenyan_coffee {


    private static Kenyan_coffee ourInstance;

    public Typeface kenyan_coffee_bold;
    public Typeface kenyan_coffee_regular;
    public Typeface kenyan_coffee_bold_italic;
    public Typeface kenyan_coffee_regular_italic;

    public static Kenyan_coffee getInstance(Context context) {
        if(ourInstance == null)
        {
            return ourInstance = new Kenyan_coffee(context);
        }
        else
        {
            return ourInstance;
        }
    }
    public static Kenyan_coffee getOurInstance()
    {
        return ourInstance;
    }

    private Kenyan_coffee(Context context) {

        kenyan_coffee_regular = Typeface.createFromAsset(context.getAssets(), "fonts/kenyan_coffee_rg.ttf");
        kenyan_coffee_bold = Typeface.createFromAsset(context.getAssets(), "fonts/kenyan_coffee_bd.ttf");
        kenyan_coffee_regular_italic = Typeface.createFromAsset(context.getAssets(), "fonts/kenyan_coffee_rg_it.ttf");
        kenyan_coffee_bold_italic = Typeface.createFromAsset(context.getAssets(), "fonts/kenyan_coffee_bd_it.ttf");
    }
}
