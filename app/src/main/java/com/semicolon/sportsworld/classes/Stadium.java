package com.semicolon.sportsworld.classes;

import com.google.android.gms.maps.model.LatLng;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Taha Malik on 08/04/2018.
 **/
public class Stadium {

    public static String MultiPurposeStatdium = "Multi";

    public String name;
    public String contact;
    public LatLng latLng;
    public Timestamp openingTime;
    public Timestamp closingTime;
    public float perHourOrPerGameFee;
    public byte[] picture;
    public ArrayList<String> sports;

    public Stadium(String name, String contact, Timestamp openingTime,
                   Timestamp closingTime, LatLng latLng, byte[] picture,
                   float perHourOrPerGameFee, String[] sports) {
        this.name = name;
        this.contact = contact;
        this.openingTime = openingTime;
        this.closingTime = closingTime;
        this.latLng = latLng;
        this.picture = picture;
        this.perHourOrPerGameFee = perHourOrPerGameFee;
        this.sports = new ArrayList<>();
        this.sports.addAll(Arrays.asList(sports));

    }

    public String[] getSports()
    {
        return (String[]) sports.toArray();
    }

}
