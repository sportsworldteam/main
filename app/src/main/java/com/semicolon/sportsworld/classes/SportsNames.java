package com.semicolon.sportsworld.classes;

/**
 * Created by Taha Malik on 08/04/2018.
 **/
public class SportsNames {

    public static String CRICKET = "CRICKET";
    public static String FOOTBALL = "FOOTBALL";
    public static String BEACHBALL = "BEACH BALL";
    public static String BASKETBALL = "BASKETBALL";
    public static String HOCKEY = "HOCKEY";
    public static String TENNIS = "TENNIS";
    public static String TABLE_TENNIS = "TABLE TENNIS";
    public static String GOLF = "GOLF";
    public static String VOLLEYBALL = "VOLLEYBALL";
    public static String BOWLING = "BOWLING";
    public static String BASEBALL = "BASEBALL";
    public static String RUGBY = "RUGBY";
    public static String SNOOKER = "SNOOKER";

}
