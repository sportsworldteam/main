package com.semicolon.sportsworld.classes.Cricket;

import com.semicolon.sportsworld.classes.PreferredArm;

/**
 * Created by Taha Malik on 08/07/2018.
 **/
public class Batsman extends Cricket {


    //Batting Orders
    public final static String OPENER = "Opening";
    public final static String TOP_ORDER = "Top Order";
    public final static String MIDDLE_ORDER = "Middle Order";
    public final static String LOWER_ORDER = "Lower Order";


    //Types of Possible Batsman
    public final static String AGGRESSIVE = "Aggressive";
    public final static String MODERATE = "MODERATE";
    public final static String DEFENCIVE = "DEFENCIVE";


    public String type;
    public String order;

    public Batsman(PreferredArm preferredArm, String order, String type) {
        super("Cricket", preferredArm);
        this.order = order;
        this.type = type;
    }



}
