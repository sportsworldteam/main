package com.semicolon.sportsworld.classes;

import android.graphics.Bitmap;
import com.google.android.gms.maps.model.LatLng;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Taha Malik on 08/04/2018.
 **/
public class Team {

    private ArrayList<String> players;
    private String teamOf;
    private String name;
    private LatLng area;
    private byte[] logo;
    private int under;
    private String contact;
    private String captain;

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }


    public Team(String name, LatLng area, byte[] logo, int under, String[] players, String teamOf, String captain, String contact) {
        this.name = name;
        this.area = area;
        this.logo = logo;
        this.teamOf = teamOf;
        this.under = under;
        this.players = new ArrayList<>();
        this.players.addAll(Arrays.asList(players));
        this.captain = captain;
        this.contact = contact;
    }
    public Team(String name, LatLng area, Bitmap logo, int under, String[] players, String teamOf) {
        this.name = name;
        this.teamOf = teamOf;
        this.area = area;
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        logo.compress(Bitmap.CompressFormat.PNG,100,stream);
        this.logo = stream.toByteArray();
        this.under = under;
        this.players = new ArrayList<>();
        this.players.addAll(Arrays.asList(players));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeamOf() {
        return teamOf;
    }

    public void setTeamOf(String teamOf) {
        this.teamOf = teamOf;
    }

    public LatLng getArea() {
        return area;
    }

    public void setArea(LatLng area) {
        this.area = area;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public int getUnder() {
        return under;
    }

    public void setUnder(int under) {
        this.under = under;
    }

    public String getCaptain() {
        return captain;
    }

    public void setCaptain(String captain) {
        this.captain = captain;
    }

    public Player[] getPlayers()
    {
        return (Player[]) players.toArray();
    }

    public void addPlayer(String username)
    {
        players.add(username);
    }

}
