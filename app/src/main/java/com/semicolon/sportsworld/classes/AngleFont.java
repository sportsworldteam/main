package com.semicolon.sportsworld.classes;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by Taha Malik on 08/03/2018.
 **/
public class AngleFont {


    private static AngleFont ourInstance;

    public Typeface angleFont;
    public Typeface squareFont;
    public Typeface wideFont;

    public static AngleFont getInstance(Context context) {
        if(ourInstance == null)
        {
            return ourInstance = new AngleFont(context);
        }
        else
        {
            return ourInstance;
        }
    }
    public static AngleFont getOurInstance()
    {
        return ourInstance;
    }

    private AngleFont(Context context) {

        angleFont = Typeface.createFromAsset(context.getAssets(), "fonts/angle1.ttf");
        squareFont = Typeface.createFromAsset(context.getAssets(), "fonts/uasquare.ttf");
        wideFont = Typeface.createFromAsset(context.getAssets(), "fonts/pirulen_rg.ttf");
    }
}
