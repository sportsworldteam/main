package com.semicolon.sportsworld.classes.Cricket;

import com.semicolon.sportsworld.classes.PreferredArm;
import com.semicolon.sportsworld.classes.Sport;

/**
 * Created by Taha Malik on 08/07/2018.
 **/
public class Cricket extends Sport {

    public static String BATSMAN = "Batsman";
    public static String BOWLER = "Bowler";
    public static String ALL_ROUNDER = "All Rounder";


    protected Cricket(String name, PreferredArm preferredArm) {
        super(name, preferredArm);
    }


    public static String[] getPlayerTypes()
    {
        return new String[]{BATSMAN, BOWLER, ALL_ROUNDER};
    }
}
