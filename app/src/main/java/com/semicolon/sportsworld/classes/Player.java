package com.semicolon.sportsworld.classes;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Taha Malik on 08/04/2018.
 **/
public class Player {

    private String firstName;
    private String lastName;
    private ArrayList<PlayerTeamDetail> details;
    private String contact;
    private byte[] picture;
    private String user_name;
    private String email;
    private LatLng latLng;

    public Player(@NonNull String firstName, @NonNull String lastName,
                  @Nullable PlayerTeamDetail[] playerTeamDetails,
                  @NonNull byte[] picture, @NonNull String user_name,
                  @NonNull String email, String contact, LatLng latLng) {

        this.latLng = latLng;
        this.contact = contact;
        this.firstName = firstName;
        this.lastName = lastName;
        this.picture = picture;
        this.user_name = user_name;
        this.email = email;
        details = new ArrayList<>();
        if(playerTeamDetails != null)
            this.details.addAll(Arrays.asList(playerTeamDetails));
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Player(@NonNull String firstName, @NonNull String lastName,
                  @Nullable PlayerTeamDetail[] playerTeamDetails,
                  @NonNull Bitmap picture, @NonNull String user_name,
                  String email, LatLng latLng) {
        this.latLng = latLng;
        this.firstName = firstName;
        this.lastName = lastName;
        details = new ArrayList<>();
        if(playerTeamDetails != null)
            this.details.addAll(Arrays.asList(playerTeamDetails));
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        picture.compress(Bitmap.CompressFormat.PNG, 100, stream);
        this.picture = stream.toByteArray();
        this.user_name = user_name;
        this.email = email;

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public PlayerTeamDetail[] getDetails()
    {
        return (PlayerTeamDetail[]) details.toArray();
    }

    public void addPlayerTeamDetail(PlayerTeamDetail playerTeamDetail)
    {
        details.add(playerTeamDetail);
    }

    public byte[] getPictureByte() {
        return picture;
    }

    public Bitmap getPictureBitmap()
    {
        return BitmapFactory.decodeByteArray(picture, 0, picture.length);
    }

//    public String getSportForPosition(String position)
//    {
//        int index = -1;
//
//        for(String s: positions)
//        {
//            if(s.equalsIgnoreCase(position)){
//                index = positions.indexOf(s);
//            }
//        }
//
//        if(index == -1)
//        {
//            return null;
//        }
//        else return sport_names.get(index);
//
//    }
//
//
//    public String getPositionForSport(String sport)
//    {
//        int index = -1;
//        for(String s: sport_names)
//        {
//            if(s.equalsIgnoreCase(sport)){
//                index = sport_names.indexOf(s);
//            }
//        }
//
//        if(index == -1)
//        {
//            return null;
//        }
//        else return positions.get(index);
//    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public void setPicture(Bitmap picture) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        picture.compress(Bitmap.CompressFormat.PNG, 100, stream);
        this.picture = stream.toByteArray();
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getEmail() {
        return email;
    }

}
