package com.semicolon.sportsworld.classes.Cricket;

import com.semicolon.sportsworld.classes.PreferredArm;

/**
 * Created by Taha Malik on 08/07/2018.
 **/
public class Bowler extends Cricket {


    public final static String FAST = "Fast";
    public final static String MEDIUM = "Medium";
    public final static String MEDIUM_FAST = "Medium Fast";
    public final static String FAST_MEDIUM = "Fast Medium";
    public final static String GOOGLY = "Google";
    public final static String OFF_SPIN = "Off Spin";
    public final static String LEG_SPIN = "Leg Spin";
    public final static String ORTHODOX = "Orthodox";

    public String type;

    public Bowler(PreferredArm preferredArm, String type) {
        super("Cricket", preferredArm);
        this.type = type;
    }

    public static String[] getBowlingTypes()
    {
        return new String[]{FAST, MEDIUM, MEDIUM_FAST, FAST_MEDIUM, GOOGLY, OFF_SPIN, LEG_SPIN, ORTHODOX};
    }


}
