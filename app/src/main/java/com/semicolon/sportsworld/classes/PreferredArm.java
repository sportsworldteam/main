package com.semicolon.sportsworld.classes;

/**
 * Created by Taha Malik on 08/07/2018.
 **/
public enum PreferredArm {
    LEFT, RIGHT, USES_BOTH;

    public String getString(PreferredArm pa)
    {
        if(pa == LEFT)
        {
            return "Left";
        }
        else if(pa == RIGHT)
        {
            return "Right";
        }
        else return "Both";
    }
}
