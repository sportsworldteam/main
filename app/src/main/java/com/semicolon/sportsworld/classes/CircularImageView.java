package com.semicolon.sportsworld.classes;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

/**
 * Created by Taha Malik on 08/04/2018.
 **/
public class CircularImageView extends AppCompatImageView {
    Bitmap src;
    Paint paint;
    final Paint paint2 = new Paint();
    public CircularImageView(Context context) {
        super(context);
        paint = new Paint();

        paint2.setAntiAlias(true);
        paint2.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));

    }

    public CircularImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CircularImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setImageDrawable(@Nullable Drawable drawable) {
        super.setImageDrawable(drawable);
        if(drawable != null)
        {
            src = ((BitmapDrawable)drawable).getBitmap();
        }
        else
            src = null;
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(bm);
//        src = bm;
//        src = getclip();
//        src = getCroppedBitmap(bm);
    }


    @Override
    protected void onDraw(Canvas canvas) {
//        canvas.drawBitmap(src,);
//        canvas.drawCircle(getWidth()/2,getHeight()/2,getWidth()/2,paint2);
        canvas.drawBitmap(src, getLeft(),getTop(),paint);
    }




    public Bitmap getclip() {
        Bitmap output = Bitmap.createBitmap(src.getWidth(),
                src.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        final Rect rect = new Rect(0, 0, src.getWidth(),
                src.getHeight());

        canvas.drawARGB(0, 0, 0, 0);
        // paint.setColor(color);
        canvas.drawCircle(src.getWidth() / 2,
                src.getHeight() / 2, src.getWidth() / 2, paint2);
        canvas.drawBitmap(src, rect, rect, paint2);
        return output;
    }
}
