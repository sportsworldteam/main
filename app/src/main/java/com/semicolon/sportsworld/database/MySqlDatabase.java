package com.semicolon.sportsworld.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;
import com.semicolon.sportsworld.classes.Player;
import com.semicolon.sportsworld.classes.PlayerTeamDetail;
import com.semicolon.sportsworld.classes.Stadium;
import com.semicolon.sportsworld.classes.Team;

import java.sql.Timestamp;
import java.util.ArrayList;

/**
 * Created by Taha Malik on 08/05/2018.
 **/
public class MySqlDatabase extends SQLiteOpenHelper {

    public final static String DATABASE_NAME = "SportsWorld";
    SportsWorldDbSchema.PlayersTable playersTable;
    SportsWorldDbSchema.PlayersTable.Cols playerTableCol;
    private SportsWorldDbSchema.TeamsTable teamsTable;
    SportsWorldDbSchema.TeamsTable.Col teamsTableCol;
    SportsWorldDbSchema.PlayerTeamDetails playerTeamDetailsTable;
    SportsWorldDbSchema.PlayerTeamDetails.Cols playerTeamDetailsTablecol;
    SportsWorldDbSchema.StadiumsTable stadiumsTable;
    SportsWorldDbSchema.StadiumsTable.Cols stadiumsTableCol;
    SportsWorldDbSchema.StadiumSportsTABLE stadiumSportsTABLE;
    SportsWorldDbSchema.StadiumSportsTABLE.Cols stadiumSportsTABLECol;

    public static MySqlDatabase DATABASE;


    public static MySqlDatabase getInstance(Context context)
    {
        if(DATABASE == null)
        {
            return DATABASE = new MySqlDatabase(context);
        }
        else return DATABASE;
    }

    private MySqlDatabase(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " +
                SportsWorldDbSchema.TeamsTable.NAME + "(" +
                SportsWorldDbSchema.TeamsTable.Col.NAME + " VARCHAR PRIMARY KEY, " +
                SportsWorldDbSchema.TeamsTable.Col.CAPTAIN + " VARCHAR, " +
                SportsWorldDbSchema.TeamsTable.Col.AVERAGE_PLAYERS_AGE + " INTEGER, "
                + SportsWorldDbSchema.TeamsTable.Col.LAT + " DOUBLE, " +
                SportsWorldDbSchema.TeamsTable.Col.LNG + " DOUBLE, " +
                SportsWorldDbSchema.TeamsTable.Col.TEAMOF + " VARCHAR, " +
                SportsWorldDbSchema.TeamsTable.Col.LOGO + " BLOB," +
                " FOREIGN KEY ("+ SportsWorldDbSchema.TeamsTable.Col.CAPTAIN + ") REFERENCES " +
                SportsWorldDbSchema.PlayersTable.NAME +"("+ SportsWorldDbSchema.PlayersTable.Cols.USERNAME +"));");

        db.execSQL("CREATE TABLE IF NOT EXISTS " +
                SportsWorldDbSchema.PlayersTable.NAME + "( " +
                SportsWorldDbSchema.PlayersTable.Cols.USERNAME + " VARCHAR PRIMARY KEY, " +
                SportsWorldDbSchema.PlayersTable.Cols.FIRST_NAME + " VARCHAR NOT NULL, " +
                SportsWorldDbSchema.PlayersTable.Cols.LAST_NAME + " VARCHAR NOT NULL, " +
                SportsWorldDbSchema.PlayersTable.Cols.EMAIL + " VARCHAR NOT NULL, " +
                SportsWorldDbSchema.PlayersTable.Cols.CONTACT + " VARCHAR, " +
                SportsWorldDbSchema.PlayersTable.Cols.LAT + " DOUBLE, " +
                SportsWorldDbSchema.PlayersTable.Cols.LNG + "DOUBLE, " +
                SportsWorldDbSchema.PlayersTable.Cols.PICTURE + " BLOB );");

        db.execSQL("CREATE TABLE IF NOT EXISTS " +
                SportsWorldDbSchema.PlayerTeamDetails.NAME + "( " +
                SportsWorldDbSchema.PlayerTeamDetails.Cols.USERNAME + " VARCHAR, " +
                SportsWorldDbSchema.PlayerTeamDetails.Cols.TEAM + " VARCHAR, " +
                SportsWorldDbSchema.PlayerTeamDetails.Cols.POSITION + "VARCHAR, " +
                "PRIMARY KEY(" + SportsWorldDbSchema.PlayerTeamDetails.Cols.USERNAME + " , " +
                SportsWorldDbSchema.PlayerTeamDetails.Cols.TEAM + "), " +
                "FOREIGN KEY("+ SportsWorldDbSchema.PlayerTeamDetails.Cols.USERNAME + ")" +
                " REFERENCES " + SportsWorldDbSchema.PlayersTable.NAME + "(" + SportsWorldDbSchema.PlayersTable.Cols.USERNAME +"), " +
                "FOREIGN KEY(" + SportsWorldDbSchema.PlayerTeamDetails.Cols.TEAM +")" +
                " REFERENCES " + SportsWorldDbSchema.TeamsTable.NAME +"("+ SportsWorldDbSchema.TeamsTable.Col.NAME +"));");

        db.execSQL("CREATE TABLE IF NOT EXISTS " +
                SportsWorldDbSchema.StadiumsTable.NAME + "(" +
                SportsWorldDbSchema.StadiumsTable.Cols.NAME + " VARCHAR PRIMARY KEY, " +
                SportsWorldDbSchema.StadiumsTable.Cols.CONTACT + " VARCHAR, " +
                SportsWorldDbSchema.StadiumsTable.Cols.LAT + " DOUBLE, " +
                SportsWorldDbSchema.StadiumsTable.Cols.LNG + " DOUBLE, " +
                SportsWorldDbSchema.StadiumsTable.Cols.OPENINGTIME + " TIMESTAMP, " +
                SportsWorldDbSchema.StadiumsTable.Cols.CLOSINGTIME + " TIMESTAMP, " +
                SportsWorldDbSchema.StadiumsTable.Cols.PER_HOUR_OR_GAME_FEE + " FLOAT, " +
                SportsWorldDbSchema.StadiumsTable.Cols.PICTURE + " BLOB);");

        db.execSQL("CREATE TABLE IF NOT EXISTS " +
                SportsWorldDbSchema.StadiumSportsTABLE.NAME + "(" +
                SportsWorldDbSchema.StadiumSportsTABLE.Cols.STADIUM_NAME + " VARCHAR, " +
                SportsWorldDbSchema.StadiumSportsTABLE.Cols.SPORTS_PLAYED + " VARCHAR, " +
                "PRIMARY KEY(" + SportsWorldDbSchema.StadiumSportsTABLE.Cols.STADIUM_NAME +
                " , " + SportsWorldDbSchema.StadiumSportsTABLE.Cols.SPORTS_PLAYED + "), " +
                "FOREIGN KEY (" + SportsWorldDbSchema.StadiumSportsTABLE.Cols.STADIUM_NAME + ") " +
                "REFERENCES " + SportsWorldDbSchema.StadiumsTable.NAME + "(" +
                SportsWorldDbSchema.StadiumsTable.Cols.NAME + "));");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void addTeam(Team team)
    {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SportsWorldDbSchema.TeamsTable.Col.NAME, team.getName());
        values.put(SportsWorldDbSchema.TeamsTable.Col.LAT, team.getArea().latitude);
        values.put(SportsWorldDbSchema.TeamsTable.Col.LNG, team.getArea().longitude);
        values.put(SportsWorldDbSchema.TeamsTable.Col.CAPTAIN, team.getCaptain());
        values.put(SportsWorldDbSchema.TeamsTable.Col.AVERAGE_PLAYERS_AGE, team.getUnder());
        values.put(SportsWorldDbSchema.TeamsTable.Col.LOGO, team.getLogo());
        values.put(SportsWorldDbSchema.TeamsTable.Col.TEAMOF, team.getTeamOf());

        db.insert(SportsWorldDbSchema.TeamsTable.NAME, null, values);
    }

    public Team getTeam(String teamName)
    {

        SQLiteDatabase database = getReadableDatabase();

        Cursor cursor = database.query(SportsWorldDbSchema.TeamsTable.NAME,
                new String[]{SportsWorldDbSchema.TeamsTable.Col.NAME,
                        SportsWorldDbSchema.TeamsTable.Col.CONTACT,
                        SportsWorldDbSchema.TeamsTable.Col.CAPTAIN,
                        SportsWorldDbSchema.TeamsTable.Col.LOGO,
                        SportsWorldDbSchema.TeamsTable.Col.TEAMOF,
                        SportsWorldDbSchema.TeamsTable.Col.LAT,
                        SportsWorldDbSchema.TeamsTable.Col.LNG,
                        SportsWorldDbSchema.TeamsTable.Col.AVERAGE_PLAYERS_AGE},
                        SportsWorldDbSchema.TeamsTable.Col.NAME + " = ? ",
                        new String[]{teamName},null,null,null);

        String name = cursor.getString(0);
        String Contact = cursor.getString(1);
        String captainUsername = cursor.getString(2);
        byte[] logo = cursor.getBlob(3);
        String sport = cursor.getString(4);
        LatLng latLng = new LatLng(cursor.getDouble(5),
                cursor.getDouble(6));
        int under = cursor.getInt(7);
        cursor.close();

        cursor = database.query(SportsWorldDbSchema.PlayerTeamDetails.NAME,
                new String[]{SportsWorldDbSchema.PlayerTeamDetails.Cols.USERNAME},
                SportsWorldDbSchema.PlayerTeamDetails.Cols.TEAM + " = ?",
                new String[]{teamName},null,null,null);

        int noOfPlayers = cursor.getCount();

        String[] players =new String[noOfPlayers];

        cursor.moveToFirst();
        for(int i = 0; i < noOfPlayers; i++)
        {
            players[i] = cursor.getString(0);
            cursor.moveToNext();
        }
        cursor.close();
        database.close();
        return new Team(name, latLng, logo, under, players, sport, captainUsername, Contact);

    }

    public Team[] getTeams()
    {
        SQLiteDatabase database = getReadableDatabase();

        Cursor cursor = database.query(SportsWorldDbSchema.TeamsTable.NAME,
                new String[]{SportsWorldDbSchema.TeamsTable.Col.NAME,
                        SportsWorldDbSchema.TeamsTable.Col.CONTACT,
                        SportsWorldDbSchema.TeamsTable.Col.CAPTAIN,
                        SportsWorldDbSchema.TeamsTable.Col.LOGO,
                        SportsWorldDbSchema.TeamsTable.Col.TEAMOF,
                        SportsWorldDbSchema.TeamsTable.Col.LAT,
                        SportsWorldDbSchema.TeamsTable.Col.LNG,
                        SportsWorldDbSchema.TeamsTable.Col.AVERAGE_PLAYERS_AGE},
                null, null, null, null, null);

        cursor.moveToFirst();
        int size = cursor.getCount();
        String[] names = new String[size];
        String[] contacts = new String[size];
        String[] captainNames = new String[size];
        ArrayList<byte[]> logos = new ArrayList<>();
        LatLng[] latLngs = new LatLng[size];
        String sports[] = new String[size];
        int[] unders = new int[size];

        for(int i = 0; i < size; i++) {
            names[i] = cursor.getString(0);
            contacts[i] = cursor.getString(1);
            captainNames[i] = cursor.getString(2);
            byte[] logo = cursor.getBlob(3);
            logos.add(logo);
            sports[i] = cursor.getString(4);
            latLngs[i] = new LatLng(cursor.getDouble(5),
                    cursor.getDouble(6));
            unders[i] = cursor.getInt(7);
            cursor.moveToNext();
        }

        cursor.close();


        Team[] teams = new Team[size];

        for(int i = 0; i < size; i++) {
            cursor = database.query(SportsWorldDbSchema.PlayerTeamDetails.NAME,
                    new String[]{SportsWorldDbSchema.PlayerTeamDetails.Cols.USERNAME},
                    SportsWorldDbSchema.PlayerTeamDetails.Cols.TEAM + " = ? ",
                    new String[]{names[i]}, null, null, null);

            int noOfPlayers = cursor.getCount();

            String[] players = new String[noOfPlayers];

            cursor.moveToFirst();
            for (int j = 0; j < noOfPlayers; j++) {
                players[j] = cursor.getString(0);
                cursor.moveToNext();
            }

            teams[i] = new Team(names[i], latLngs[i], logos.get(i), unders[i],
                    players, sports[i], captainNames[i], contacts[i]);
;
        }
        cursor.close();
        database.close();

        return teams;
    }

    public void addPlayer(Player player)
    {
        SQLiteDatabase database = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SportsWorldDbSchema.PlayersTable.Cols.USERNAME, player.getUser_name());
        values.put(SportsWorldDbSchema.PlayersTable.Cols.FIRST_NAME, player.getFirstName());
        values.put(SportsWorldDbSchema.PlayersTable.Cols.LAST_NAME, player.getLastName());
        values.put(SportsWorldDbSchema.PlayersTable.Cols.EMAIL, player.getEmail());
        values.put(SportsWorldDbSchema.PlayersTable.Cols.PICTURE, player.getPictureByte());
        values.put(SportsWorldDbSchema.PlayersTable.Cols.LAT, player.getLatLng().latitude);
        values.put(SportsWorldDbSchema.PlayersTable.Cols.LNG, player.getLatLng().longitude);
        values.put(SportsWorldDbSchema.PlayersTable.Cols.CONTACT, player.getContact());

        database.insert(SportsWorldDbSchema.PlayersTable.NAME, null, values);
        database.close();
        addPlayerDetails(player.getDetails(),player.getUser_name());
    }

    public Player getPlayer(String username)
    {
        SQLiteDatabase database = getReadableDatabase();

        Cursor cursor = database.query(SportsWorldDbSchema.PlayersTable.NAME,
                new String[]{SportsWorldDbSchema.PlayersTable.Cols.USERNAME,
                        SportsWorldDbSchema.PlayersTable.Cols.CONTACT,
                        SportsWorldDbSchema.PlayersTable.Cols.EMAIL,
                        SportsWorldDbSchema.PlayersTable.Cols.FIRST_NAME,
                        SportsWorldDbSchema.PlayersTable.Cols.LAST_NAME,
                        SportsWorldDbSchema.PlayersTable.Cols.LAT,
                        SportsWorldDbSchema.PlayersTable.Cols.LNG,
                        SportsWorldDbSchema.PlayersTable.Cols.PICTURE},
                SportsWorldDbSchema.PlayersTable.Cols.USERNAME + " = ? ",
                new String[]{username},null,null,null);

        String user_name = cursor.getString(0);
        String contact = cursor.getString(1);
        String email = cursor.getString(2);
        String firstName = cursor.getString(3);
        String lastName = cursor.getString(4);
        LatLng latLng = new LatLng(cursor.getDouble(5),
                cursor.getDouble(6));
        byte[] picture = cursor.getBlob(7);

        cursor.close();
        cursor = database.query(SportsWorldDbSchema.PlayerTeamDetails.NAME,
                new String[]{SportsWorldDbSchema.PlayerTeamDetails.Cols.TEAM,
                        SportsWorldDbSchema.PlayerTeamDetails.Cols.POSITION},
                SportsWorldDbSchema.PlayerTeamDetails.Cols.USERNAME + " = ? ",
                new String[]{username},null,null,null);

        int size = cursor.getCount();

        PlayerTeamDetail[] playerTeamDetails = new PlayerTeamDetail[size];

        cursor.moveToFirst();
        for(int i = 0; i < size; i++)
        {
            playerTeamDetails[i] = new PlayerTeamDetail(cursor.getString(0),
                    cursor.getString(1));
            cursor.moveToNext();
        }
        cursor.close();
        return new Player(firstName, lastName, playerTeamDetails, picture,
                user_name, email, contact, latLng);

    }

    public Player[] getPlayers(){
        SQLiteDatabase database = getReadableDatabase();
        Player[] players;


        Cursor cursor = database.query(SportsWorldDbSchema.PlayersTable.NAME,
                new String[]{SportsWorldDbSchema.PlayersTable.Cols.USERNAME,
                        SportsWorldDbSchema.PlayersTable.Cols.CONTACT,
                        SportsWorldDbSchema.PlayersTable.Cols.EMAIL,
                        SportsWorldDbSchema.PlayersTable.Cols.FIRST_NAME,
                        SportsWorldDbSchema.PlayersTable.Cols.LAST_NAME,
                        SportsWorldDbSchema.PlayersTable.Cols.LAT,
                        SportsWorldDbSchema.PlayersTable.Cols.LNG,
                        SportsWorldDbSchema.PlayersTable.Cols.PICTURE},
                null,
                null, null, null, null);

        int size1 = cursor.getCount();
        players = new Player[size1];

        cursor.moveToFirst();

        for(int j = 0; j < size1; j++) {
            String user_name = cursor.getString(0);
            String contact = cursor.getString(1);
            String email = cursor.getString(2);
            String firstName = cursor.getString(3);
            String lastName = cursor.getString(4);
            LatLng latLng = new LatLng(cursor.getDouble(5),
                    cursor.getDouble(6));
            byte[] picture = cursor.getBlob(7);

            Cursor cursor1 = database.query(SportsWorldDbSchema.PlayerTeamDetails.NAME,
                    new String[]{SportsWorldDbSchema.PlayerTeamDetails.Cols.TEAM,
                            SportsWorldDbSchema.PlayerTeamDetails.Cols.POSITION},
                    SportsWorldDbSchema.PlayerTeamDetails.Cols.USERNAME + " = ? ",
                    new String[]{user_name}, null, null, null);

            int size = cursor.getCount();

            PlayerTeamDetail[] playerTeamDetails = new PlayerTeamDetail[size];

            cursor1.moveToFirst();
            for (int i = 0; i < size; i++) {
                playerTeamDetails[i] = new PlayerTeamDetail(cursor1.getString(0),
                        cursor.getString(1));
                cursor1.moveToNext();
            }
            cursor1.close();
            players[j] = new Player(firstName, lastName, playerTeamDetails, picture,
                    user_name, email, contact, latLng);
            cursor.moveToNext();
        }

        database.close();
        cursor.close();
        return players;
    }

    public void addPlayerDetails(@NonNull PlayerTeamDetail[] playerTeamDetail, String username)
    {
        if(playerTeamDetail.length > 0)
        {
            SQLiteDatabase database = getWritableDatabase();
            for(PlayerTeamDetail p : playerTeamDetail) {
                ContentValues values = new ContentValues();
                values.put(SportsWorldDbSchema.PlayerTeamDetails.Cols.USERNAME, username);
                values.put(SportsWorldDbSchema.PlayerTeamDetails.Cols.TEAM, p.team_name);
                values.put(SportsWorldDbSchema.PlayerTeamDetails.Cols.POSITION, p.position);


                database.insert(SportsWorldDbSchema.PlayerTeamDetails.NAME,null,values);
            }
            database.close();
        }
    }

    public PlayerTeamDetail[] getPlayerDetails(String username)
    {
        SQLiteDatabase database = getReadableDatabase();

        Cursor cursor = database.query(SportsWorldDbSchema.PlayerTeamDetails.NAME,
                new String[]{SportsWorldDbSchema.PlayerTeamDetails.Cols.TEAM,
                        SportsWorldDbSchema.PlayerTeamDetails.Cols.POSITION},
                SportsWorldDbSchema.PlayerTeamDetails.Cols.USERNAME + " = ? ",
                new String[]{username},null,null,null);

        int size = cursor.getCount();

        PlayerTeamDetail[] playerTeamDetails = new PlayerTeamDetail[size];

        cursor.moveToFirst();
        for(int i = 0; i < size; i++)
        {
            playerTeamDetails[i] = new PlayerTeamDetail(cursor.getString(0),
                    cursor.getString(1));
            cursor.moveToNext();
        }

        cursor.close();
        database.close();
        return playerTeamDetails;
    }

    public void addSinglePlayerDetails(PlayerTeamDetail p, String username)
    {
        SQLiteDatabase database = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SportsWorldDbSchema.PlayerTeamDetails.Cols.USERNAME, username);
        values.put(SportsWorldDbSchema.PlayerTeamDetails.Cols.TEAM, p.team_name);
        values.put(SportsWorldDbSchema.PlayerTeamDetails.Cols.POSITION, p.position);

        database.insert(SportsWorldDbSchema.PlayerTeamDetails.NAME,null,values);
        database.close();
    }

    public void addStadium(Stadium stadium){

        SQLiteDatabase database = getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(SportsWorldDbSchema.StadiumsTable.Cols.NAME, stadium.name);
        values.put(SportsWorldDbSchema.StadiumsTable.Cols.CONTACT, stadium.contact);
        values.put(SportsWorldDbSchema.StadiumsTable.Cols.LAT, stadium.latLng.latitude);
        values.put(SportsWorldDbSchema.StadiumsTable.Cols.LNG, stadium.latLng.longitude);
        values.put(SportsWorldDbSchema.StadiumsTable.Cols.OPENINGTIME, stadium.openingTime.toString());
        values.put(SportsWorldDbSchema.StadiumsTable.Cols.CLOSINGTIME, stadium.closingTime.toString());
        values.put(SportsWorldDbSchema.StadiumsTable.Cols.PER_HOUR_OR_GAME_FEE, stadium.perHourOrPerGameFee);
        values.put(SportsWorldDbSchema.StadiumsTable.Cols.PICTURE, stadium.picture);
        database.insert(SportsWorldDbSchema.StadiumsTable.NAME, null, values);
        database.close();
        addStadiumSport(stadium.getSports(), stadium.name);

    }

    public Stadium getStadium(String name)
    {
        SQLiteDatabase database = getReadableDatabase();

        Cursor cursor = database.query(SportsWorldDbSchema.StadiumsTable.NAME,
                new String[]{SportsWorldDbSchema.StadiumsTable.Cols.LAT,
                        SportsWorldDbSchema.StadiumsTable.Cols.LNG,
                        SportsWorldDbSchema.StadiumsTable.Cols.CLOSINGTIME,
                        SportsWorldDbSchema.StadiumsTable.Cols.OPENINGTIME,
                        SportsWorldDbSchema.StadiumsTable.Cols.PICTURE,
                        SportsWorldDbSchema.StadiumsTable.Cols.PER_HOUR_OR_GAME_FEE,
                        SportsWorldDbSchema.StadiumsTable.Cols.CONTACT},
                SportsWorldDbSchema.StadiumsTable.Cols.NAME + " = ? ",
                new String[]{name},null, null, null);

        LatLng latLng = new LatLng(cursor.getDouble(0), cursor.getDouble(1));
        Timestamp closingTime = Timestamp.valueOf(cursor.getString(2));
        Timestamp openingTime = Timestamp.valueOf(cursor.getString(3));
        byte[] picture = cursor.getBlob(4);
        float per_hr_fee = cursor.getFloat(5);
        String contact = cursor.getString(6);
        String[] sports = getStadiumSport(name);

        cursor.close();
        database.close();

        return new Stadium(name, contact, openingTime, closingTime, latLng,
                picture, per_hr_fee, sports);


    }

    public Stadium[] getStadiums()
    {
        SQLiteDatabase database = getReadableDatabase();

        Cursor cursor = database.query(SportsWorldDbSchema.StadiumsTable.NAME,
                new String[]{SportsWorldDbSchema.StadiumsTable.Cols.LAT,
                        SportsWorldDbSchema.StadiumsTable.Cols.LNG,
                        SportsWorldDbSchema.StadiumsTable.Cols.CLOSINGTIME,
                        SportsWorldDbSchema.StadiumsTable.Cols.OPENINGTIME,
                        SportsWorldDbSchema.StadiumsTable.Cols.PICTURE,
                        SportsWorldDbSchema.StadiumsTable.Cols.PER_HOUR_OR_GAME_FEE,
                        SportsWorldDbSchema.StadiumsTable.Cols.CONTACT,
                        SportsWorldDbSchema.StadiumsTable.Cols.NAME},
                null, null, null, null, null);


        int size = cursor.getCount();
        Stadium[] stadiums = new Stadium[size];

        cursor.moveToFirst();

        for(int i = 0; i < size; i++) {

            String name = cursor.getString(7);
            LatLng latLng = new LatLng(cursor.getDouble(0), cursor.getDouble(1));
            Timestamp closingTime = Timestamp.valueOf(cursor.getString(2));
            Timestamp openingTime = Timestamp.valueOf(cursor.getString(3));
            byte[] picture = cursor.getBlob(4);
            float per_hr_fee = cursor.getFloat(5);
            String contact = cursor.getString(6);
            String[] sports = getStadiumSport(name);
            stadiums[i] = new Stadium(name, contact, openingTime, closingTime, latLng,
                    picture, per_hr_fee, sports);
            cursor.moveToNext();
        }

        cursor.close();
        database.close();
        return stadiums;

    }

    public void addStadiumSport(String[] strings, String stadium)
    {
        SQLiteDatabase database = getWritableDatabase();
        for(String s : strings)
        {
            ContentValues values = new ContentValues();
            values.put(SportsWorldDbSchema.StadiumSportsTABLE.Cols.STADIUM_NAME,stadium);
            values.put(SportsWorldDbSchema.StadiumSportsTABLE.Cols.SPORTS_PLAYED,s);

            database.insert(SportsWorldDbSchema.StadiumSportsTABLE.NAME,
                    null, values);
        }
    }

    public String[] getStadiumSport(String stadium)
    {
        SQLiteDatabase database = getReadableDatabase();

        Cursor cursor = database.query(SportsWorldDbSchema.StadiumSportsTABLE.NAME,
                new String[]{SportsWorldDbSchema.StadiumSportsTABLE.Cols.SPORTS_PLAYED},
                SportsWorldDbSchema.StadiumSportsTABLE.Cols.STADIUM_NAME + " = ? ",
                new String[]{stadium}, null, null, null);

        int size = cursor.getCount();
        String[] sports = new String[size];

        cursor.moveToFirst();
        for(int i = 0; i < size; i++)
        {
            sports[i] = cursor.getString(0);
            cursor.moveToNext();
        }

        cursor.close();
        database.close();

        return sports;
    }

}
