package com.semicolon.sportsworld.database;

/**
 * Created by Taha Malik on 08/05/2018.
 **/
public class SportsWorldDbSchema {

    public static class TeamsTable{

        public static String NAME = "TEAMS_TABLE";

        public static class Col {

            public static String NAME = "NAME";
            public static String CONTACT  = "CONTACT";
            public static String CAPTAIN = "CAPTAIN";
            public static String LOGO = "LOGO";
            public static String LAT = "LAT";
            public static String LNG = "LNG";
            public static String TEAMOF = "TEAM_OF";
            public static String AVERAGE_PLAYERS_AGE = "AVG_AGE";
        }

    }


    public static class PlayersTable{
        public static String NAME = "PLAYER_TABLE";

        public static class Cols{

            public static String USERNAME = "USERNAME";
            public static String CONTACT = "CONTACT";
            public static String EMAIL = "EMAIL";
            public static String LAT = "LATITUDE";
            public static String LNG = "LONGITUDE";
            public static String FIRST_NAME = "FIRST_NAME";
            public static String LAST_NAME = "LAST_NAME";
            public static String PICTURE = "PICTURE";

        }

    }

    public static class PlayerTeamDetails{
        public static String NAME = "PLAYER_TEAM_DETAILS_TABLE";

        public static class Cols{
            public static String USERNAME = "USERNAME";
            public static String TEAM = "TEAM";
            public static String POSITION = "POSITION";
        }
    }

    public static class StadiumsTable{
        public static String NAME = "STADIUM_TABLE";


        public static class Cols{

            public static String NAME = "NAME";
            public static String CONTACT = "CONTACT";
            public static String LAT = "LATITUDE";
            public static String LNG = "LONGITUDE";
            public static String OPENINGTIME = "OPENING_TIME";
            public static String CLOSINGTIME = "CLOSING_TIME";
            public static String PER_HOUR_OR_GAME_FEE = "PER_HOUR_OR_GAME_FEE";
            public static String PICTURE = "PICTURE";
        }
    }

    public static class StadiumSportsTABLE{
        public static String NAME = "STADIUM SPORTS CATEGORY";

        public static class Cols{
            public static String STADIUM_NAME = "STADIUM NAME";
            public static String SPORTS_PLAYED = "SPORTS PLAYED";
        }
    }
}
